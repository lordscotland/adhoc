%hook MICodeSigningVerifier
@interface MICodeSigningVerifier
@property(assign) BOOL allowAdhocSigning;
@end
-(id)initWithBundle:(id)bundle {
  if((self=%orig)){self.allowAdhocSigning=YES;}
  return self;
}
%end
